class something {
	constructor(name) {
		this.name = name;
		this.age = null;
		console.log(`${name} created`)
	}

	addAge(age) {
		this.age = age;
		console.log("Object updated:", this)
	}

}

function start() {
	let me = new something("Buki");
	me.addAge(27);
}