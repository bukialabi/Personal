/*
    To create a function that takes a positive integer number
    and returns the next bigger number formed by the same digits:
    https://www.codewars.com/kata/55983863da40caa2c900004e/train/javascript
*/

let result = -1;
let resultText = 'No result found';

function nextBigger(num){
    let numNotFound = true;
    num = Number.parseInt(num) //to mirror test input
    if(cannotIncrease(num)){
        document.getElementById('result').innerHTML = `Result: <b>${result}</b> <p>${resultText}</p>`;
        return -1;
    }else{
        let currentNum = num + 1;
        while(numNotFound){
            if(hasSameDigits(num, currentNum)){
                result = currentNum;
                resultText = "Number Found";
                document.getElementById('result').innerHTML = `Result: <b>${result}</b> <p>${resultText}</p>`;
                return currentNum;
            }
            else
                currentNum++;
        }
    }
}


function cannotIncrease(num){
  if(repeatedDigits(num)){
     result = -1;
     resultText = `${num} cannot be rearranged`;
     return true;
  }
  if(highestToLowestDigits(num)){
      result = -1;
      resultText  = `${num} is already optimized`;
      return true;
  }
  return false;
}

function repeatedDigits(num){
    if(num < 10) return true;
    num = num.toString();
    for(digit of num)
        if(digit != num[0]) return false;
    return true;
}

function highestToLowestDigits(num){
    if(getSortedDigits(num) == num)
        return true;
    return false;
}

function getSortedDigits(num){
    let sortedNum = num.toString().split('')
    sortedNum = sortedNum.sort((a,b) => {return b-a;})
    sortedNum = sortedNum.join('');
    return Number.parseInt(sortedNum);
}

function hasSameDigits(num1, num2){
    return getSortedDigits(num1) == getSortedDigits(num2);
}

//if can be increased
/*
  Can't be increased if:
  * single digit
  * string of same digits
  * digits sorted highest to lowest: string.split; array.sort; array.join; result = original?
*/
  //while larger num not found
    //increment & test for same digits

//Has Same digits
/*
  strings to arrays
  sort arrays: sortedNum = num.toString().split('').sort()
  array1.toString == array2.toString
*/
