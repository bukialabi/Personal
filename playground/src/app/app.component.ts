import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    this.display(7);
  }

  display(n) {
    if (n < 0) return;
    console.log('n is ', n);
    this.display(n - 1);
  }
}
