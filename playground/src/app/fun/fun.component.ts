import { Component, OnInit } from '@angular/core';

import { CrudService } from '../services/crud/crud.service';

@Component({
  selector: 'app-fun',
  templateUrl: './fun.component.html',
  styleUrls: ['./fun.component.css'],
  providers: [CrudService]
})
export class FunComponent implements OnInit {
    root: node = {
        child: null,
        parent: null,
        content: "root"
    };

    tree = { }

  constructor(private _crud: CrudService) { }

  ngOnInit() {
      this.tree[this.root.content] = this.root;
      let newChild:node = {
          child: null,
          parent: null,
          content: "child 1"
      };
      this.addChild(this.root, newChild);
      console.log("Our tree: ", this.tree);
      console.log("The child of the root is: ", this.getChild(this.root));
  }

  add(num1, num2){
      return num1 + num2;
  }

  addChild(parentNode:node, childNode:node){
      parentNode.child = childNode.content;
      childNode.parent = parentNode.content;
      this.tree[childNode.content] = childNode;
  }

  removeChild(){

  }

  getParent(){

  }

  getChild(node){
      return this.tree[node.child];
  }

}

interface node{
    content: string;
    parent: string;
    child: string;
}
