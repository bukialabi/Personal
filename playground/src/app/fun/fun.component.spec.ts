import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunComponent } from './fun.component';

describe('FunComponent', () => {
  let component: FunComponent;
  let fixture: ComponentFixture<FunComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should add 2 numbers correctly', () =>{
      expect(component.add(7,2)).toBe(9);
  })
});
