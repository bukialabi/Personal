import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FunComponent } from './fun/fun.component';

const routes: Routes = [
    {
        path: 'fun',
        component: FunComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
