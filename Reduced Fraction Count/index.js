let factorSet;

function enterProperFractions(event){
    if(event.code == 'Enter')
        properFractions(document.getElementById('numberInput').value);
}

function properFractions(num){
  factorSet = [];
  let result = 0;
  console.log(`You entered ${num}`)
  if(num > 2){
      for(let currentNum = 2; currentNum < num; currentNum++){
          console.log("Testing ", currentNum)
          if(num % currentNum == 0){
              addToSet(currentNum)
              addMultiplesToSet(currentNum, num)
          }
      }
      result = num - 1 - factorSet.length;
  }else{
      if(num > 0)
        result = num - 1;
      else result = 0;
  }
  console.log("Factor set: ", factorSet)
  document.getElementById('result').innerHTML = result;
}

function addToSet(num){
    if(!factorSet.includes(num)){
        factorSet.push(num);
    }
}

function addMultiplesToSet(factor, limit){
    let multiple;
    for(let i = 2; i < limit; i++){
        multiple = factor * i;
        if(multiple < limit)
            addToSet(multiple)
    }
}

/*


Finding factors of n:
- iterate from 2 to the number
    - If the number % currentNum is 0
        - Add number to set (if not in set)
        - Add multiples of the currentNumber to set, starting at currentNum * 2 until multiple exceeds or equals number (if not in set)
        - Add number/currentNumber to set (if not in set)

Answer = (nfumber - 1) - set.length


*/
